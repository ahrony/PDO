<?php
namespace MobileApp\Seip50\Mobile;
use PDO;
class Mobile
{
    public $id = '';
    public $title = '';
    public $data = '';
    public $conn = '';
    public $host = 'localhost';
    public $db = 'php27';
    public $username = 'root';
    public $password = '';

    public function __construct()
    {
        session_start();
        echo "<pre>";
        $this->conn = new PDO("mysql:host=$this->host;dbname=$this->db" ,$this->username, $this->password);
    }

    public function prepare($data = '')
    {
        if (array_key_exists('Mobile_model', $data)) {
            $this->title = $data['Mobile_model'];
        }
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }


        return $this;
    }

    public function store()
    {
        try{
            $query = "INSERT INTO `mobiles` (title) VALUES (:t)";
            $stmt = $this->conn->prepare($query);
                $stmt->execute(array(
                    ':t' => $this->title
                ));
                header('location:index.php');
        } catch (Exception $ex) {
            echo 'Error: ' . $ex->getMessage();
        }
    }

    public function index()
    {
        try{
            $query = "SELECT * FROM `mobiles`";
            $q = $this->conn->query($query) or die(mysql_error());
            while($row = $q->fetch(PDO::FETCH_ASSOC)) {
                $this->data[] = $row;
            }
            
        } catch (Exception $ex) {
            echo 'Error: ' . $ex->getMessage();
        }
        return $this->data;
    }

    public function show()
    {   
        try{
            $query = "SELECT * FROM `mobiles` WHERE id=" . $this->id;
            $q = $this->conn->query($query) or die(mysql_error());
            $row = $q->fetch(PDO::FETCH_ASSOC);
            
        } catch (Exception $ex) {
            echo 'Error: ' . $ex->getMessage();
        }
        return $row;
    }
    
    public function delete() {
        
        try{
            $query = "DELETE FROM `php27`.`mobiles` WHERE `mobiles`.`id` =" . $this->id;
            $q = $this->conn->exec($query);
            header('location:index.php');
        } catch (Exception $ex) {
            echo 'Error: ' . $ex->getMessage();
        }
    }
    
    public function update(){
       try{
            $query = "UPDATE `php27`.`mobiles` SET `title` = '$this->title' WHERE `mobiles`.`id` =".$this->id;
        
        $q = $this->conn->prepare($query);
        $q->execute(array(
                    
                    ':t' => $this->title
                ));
        
        header('location:index.php');
       } catch (Exception $ex) {

       }
       
        
    }
}












