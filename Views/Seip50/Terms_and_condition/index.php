<a href="create.php">Add Data</a>

<?php
include_once("../../../vendor/autoload.php");
use App\Seip50\Terms_and_condition\Checked;

$ob = new Checked();
$data = $ob->index();

//print_r($data);
?>
<?php
if (isset($_SESSION['Message']) && !empty($_SESSION['Message'])) {
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}

?>
<table border="1">
    <tr>

        <th>Id</th>
        <th>Data</th>
        <th>status</th>
        <th colspan="3">Action</th>
    </tr>
    <?php
    
    if(isset($data) && !empty($data)){

    foreach ($data as $item) {

        ?>
        <tr>
            <td><?php echo $item['id'] ?></td>
            <td><?php echo $item['title'] ?></td>
            <?php
            if($item['status'] == "checked"){ ?>
                <td><input type="checkbox" name="chkbox" value="checked" checked></td>
            <?php }else{ ?>
                <td><input type="checkbox" name="chkbox" value="checked"></td>
            <?php }?>
            <td><a href="show.php?id=<?php echo $item['id']?>">View</a></td>
            <td><a href="edit.php?id=<?php echo $item['id']?>">Edit</td>
            <td><a href="delete.php?id=<?php echo $item['id']?>">Delete</td>
        </tr>
    <?php }} ?>
</table>


