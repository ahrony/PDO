<?php
include_once("../../../vendor/autoload.php");
use App\Seip50\Terms_and_condition\Checked;

$obj = new Checked();
$data = $obj ->prepare($_GET)->show();
?>
<html>
<head>
    <title>Terms and Condition</title>
</head>
<body>
<fieldset>
    <legend>Update Your data</legend>
    <form action="update.php" method="post">
        <?php
        if (isset($_SESSION['Message']) && !empty($_SESSION['Message'])) {
            echo $_SESSION['Message'];
            unset($_SESSION['Message']);
        }

        ?>
        <label>
            Update Your data
        </label>
        <input type="text" name="name" value="<?php echo $data['title']?>">
        <br>
        <?php
            if($data['status'] == 'checked'){ ?>
        <input type="checkbox" name="chkbox" value="checked" checked>Terms & Condition
            <?php }else{ ?>
                <input type="checkbox" name="chkbox" value="Unchecked">Terms & Condition
            <?php } ?>
        <br>
        <input type="submit" value="Submit">
        <input type="hidden" name="id" value="<?php echo $data['id'];?>">
    </form>
</fieldset>
</body>
</html>
