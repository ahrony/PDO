<?php
include_once("../../../vendor/autoload.php");
use GenderApp\Seip50\Gender\Gender;

$ob = new Gender();
$data = $ob->prepare($_GET)->show();

?>
<table border="1">
    <tr>
        <th>ID</th>
        <th>Title</th>
        <th>Gender</th>
    </tr>
    <tr>
        <td><?php echo $data['Id'] ?></td>
        <td><?php echo $data['Title'] ?></td>
        <td><?php echo $data['Gender'] ?></td>
    </tr>
</table>