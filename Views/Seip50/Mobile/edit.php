<?php
include_once("../../../vendor/autoload.php");

use MobileApp\Seip50\Mobile\Mobile;

$obj = new Mobile();

$data = $obj ->prepare($_GET)->show();

?>
<?php

?>
<html>
<head>
    <title>Favorite Mobile Models</title>
</head>
<body>
<fieldset>
    <legend>Update favorite Mobile Models</legend>
    <form action="update.php" method="post">
        <?php
        if (isset($_SESSION['Message']) && !empty($_SESSION['Message'])) {
            echo $_SESSION['Message'];
            unset($_SESSION['Message']);
        }

        ?>
        <label>
            Update Your Favorite Mobile Model
        </label>
        <input type="text" name="Mobile_model" value="<?php echo $data['title']?>">
        <input type="submit" value="Add">
        <input type="hidden" name="id" value="<?php echo $data['id'];?>">
    </form>
</fieldset>
</body>
</html>
